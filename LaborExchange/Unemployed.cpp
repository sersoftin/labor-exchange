#include "Unemployed.h"

Unemployed::Unemployed()
	: sex_(), 
	age_(0),
	education_(),
	last_exp_(0),
	total_exp_(0),
	want_wage_(0)
{
	// �������� ������
	memset(family_, 0, sizeof(family_));
	memset(profession_, 0, sizeof(profession_));
	memset(post_, 0, sizeof(post_));
	memset(want_post_, 0, sizeof(want_post_));
}

Unemployed::Unemployed(char* family, Sex sex, unsigned age, Education education, char* profession, unsigned exp, unsigned total_exp, char* post, unsigned want_wage, char* want_post, tm date_of_add)
	: sex_(sex),
	age_(age),
	education_(education),
	last_exp_(exp),
	total_exp_(total_exp),
	want_wage_(want_wage),
	date_of_add_(date_of_add)
{
	// �������� ������
	memset(family_, 0, sizeof(family_));
	memset(profession_, 0, sizeof(profession_));
	memset(post_, 0, sizeof(post_));
	memset(want_post_, 0, sizeof(want_post_));
	// �������� � ��������� ������ ��������� �� ������������
	memcpy(family_, family, strlen(family));
	memcpy(profession_, profession, strlen(profession));
	memcpy(post_, post, strlen(post));
	memcpy(want_post_, want_post, strlen(want_post));
}

Unemployed::~Unemployed()
{
}

char* Unemployed::getFamily() // ��������� �������
{
	return family_;
}

Sex Unemployed::getSex() const // ��������� ����
{
	return sex_;
}

unsigned Unemployed::getAge() const // ��������� ��������
{
	return age_;
}

Education Unemployed::getEducation() const // ��������� �����������
{
	return education_;
}

char* Unemployed::getProfession() // ��������� ���������
{
	return profession_;
}

void Unemployed::setFamily(char* family) // ��������� �������
{
	memset(family_, 0, sizeof(family_));
	memcpy(family_, family, strlen(family));
}

void Unemployed::setSex(const Sex sex) // ��������� ����
{
	sex_ = sex;
}

void Unemployed::setAge(const unsigned age) // ��������� ��������
{
	age_ = age;
}

void Unemployed::setEducation(const Education education) // ��������� �����������
{
	education_ = education;
}

void Unemployed::setProfession(char* profession) // ��������� ���������
{
	memset(profession_, 0, sizeof(profession_));
	memcpy(profession_, profession, strlen(profession));
}

void Unemployed::setPost(char* post)
{
	memset(post_, 0, sizeof(post_));
	memcpy(post_, post, strlen(post));
}

char* Unemployed::getPost()
{
	return post_;
}

unsigned Unemployed::getLastExp() const
{
	return last_exp_;
}

void Unemployed::setLastExp(const unsigned exp)
{
	last_exp_ = exp;
}

unsigned Unemployed::getTotalExp() const
{
	return total_exp_;
}

void Unemployed::setTotalExp(const unsigned total_exp)
{
	total_exp_ = total_exp;
}

unsigned Unemployed::getWantWage() const
{
	return want_wage_;
}

void Unemployed::setWantWage(const unsigned want_wage)
{
	want_wage_ = want_wage;
}

void Unemployed::setWantPost(char* want_profession) // ��������� �������� ���������
{
	memset(want_post_, 0, sizeof(want_post_));
	memcpy(want_post_, want_profession, strlen(want_profession));
}

char* Unemployed::getWantPost() // ��������� �������� ���������
{
	return want_post_;
}

tm Unemployed::getDateOfAdd() const // ��������� ������� ����������
{
	return date_of_add_;
}

void Unemployed::setDateOfAdd(const tm& date_of_add) // ��������� ������� ����������
{
	date_of_add_ = date_of_add;
}
