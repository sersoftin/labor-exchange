#pragma once
#include <string>

class DemandedComparator // ���������� ��� ���������� � �������������� ��������
{
public:
	bool operator()(std::pair<std::string, int> const& left, std::pair<std::string, int> const& right) const
	{
		return left.second > right.second;
	}
};

