#pragma once

enum Sex : int
{
	Male,
	Female,
	Any
};

enum Education : int
{
	Preschool, // ���������� �����������;
	PrimaryGeneral, // ��������� ����� �����������;
	BasicGeneral, // �������� ����� �����������;
	Secondary, // ������� ����� �����������;
	SecondaryVocation, // ������� ���������������� �����������;
	HigherBachelor, // ������ ����������� � �����������;
	HigherSpecialtyMaster, // ������ ����������� � �����������, ������������;
	HigherEducationTrainingOfHighlyQualifiedPersonnel // ������ ����������� � ���������� ������ ������ ������������;
};