#pragma once
#include <string>
#include "Enums.h"

class Unemployed
{
public:

	Unemployed();
	Unemployed(char* family, Sex sex, unsigned age, Education education, char* profession, unsigned exp, unsigned total_exp, char* post, unsigned want_wage, char* want_profession, tm date_of_add);

	~Unemployed();

	char* getFamily();
	Sex getSex() const;
	unsigned getAge() const;
	Education getEducation() const;
	char* getProfession();

	void setFamily(char* family);
	void setSex(const Sex sex);
	void setAge(const unsigned age);
	void setEducation(const Education education);
	void setProfession(char* profession);
	void setPost(char* post);
	char* getPost();
	unsigned getLastExp() const;
	void setLastExp(const unsigned exp);
	unsigned getTotalExp() const;
	void setTotalExp(const unsigned total_exp);
	unsigned getWantWage() const;
	void setWantWage(const unsigned want_wage);
	char* getWantPost();
	void setWantPost(char* want_profession);
	tm getDateOfAdd() const;
	void setDateOfAdd(const tm& date_of_add);

private:
	char family_[20];
	Sex sex_;
	unsigned int age_;
	Education education_;
	char profession_[30];
	char post_[30];
	unsigned int last_exp_;
	unsigned int total_exp_;
	char want_post_[30];
	unsigned int want_wage_;
	tm date_of_add_;
};

