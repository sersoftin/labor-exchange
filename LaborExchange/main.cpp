#include "Printer.h"
#include <Windows.h>


int main()
{
	//SetConsoleTitle(L"�.�. ������ - ���ZS-311");
	setlocale(LC_ALL, "rus");
	std::cout << "Hello, World!" << std::endl;
	while (true)
	{
		std::cin.clear();
		std::cin.sync();
		Printer::printMenu(); // ������ ����
		auto choice = 0;
		std::cin >> choice; // ��������� �����, ������� ��� ������������
		switch (choice)
		{
			case 0:
				return 0;
			case 1:
				Printer::printUnemployed(); // �������� ����������� � �������
				break;
			case 2:
				Printer::addUnemployed(); // �������� ������ ���������� ������������
				break;
			case 3:
				Printer::removeUnemployed(); // ������� ������������ �� ������
				break;
			case 4:
				Printer::printVacancies(); // �������� ��������
				break;
			case 5:
				Printer::addVacancy(); // �������� ������ ���������� ��������
				break;
			case 6:
				Printer::removeVacancy(); // ������� ��������
				break;
			case 7:
				Printer::printDemandedProfessions(); // �������� ������ �������������� ���������
				break;
			case 8:
				Printer::printNotDemandedProfessions(); // �������� ������ ���������������� ���������
				break;
			case 9:
				Printer::printUnfilledVacancies(7); // �������� ������������� �������� �� ��������� 7 ����
				break;
			case 10:
				Printer::printUnfilledVacancies(30); // �������� ������������� �������� �� ��������� 30 ����
				break;
			case 11:
				Printer::printVacanciesByFamily(); // �������� ������ �������� �� ������� ������������
				break;
			case 12:
				Database::getInstance().clearAll(); // ������ ����
				break;
			case 13:
				Database::getInstance().loadDataFromFile(); // ������ ���� �� ������
				break;
			case 14:
				Database::getInstance().saveDataToFile(); // ��������� ���� � �����
				break;
			default:
				std::cout << "������ ��������� ��� �� �����." << std::endl;
				break;
		}
	}
}