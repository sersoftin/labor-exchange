#pragma once
#include <vector>
#include <fstream>
#include "Vacancy.h"
#include "Unemployed.h"

class Database
{
public:
	static Database& getInstance() // ����� ��� ��������
	{
		static Database database;
		return database;
	}

	void loadDataFromFile();
	void saveDataToFile();

	std::vector<Vacancy> getVacancies() const;
	void addVacancy(Vacancy vacancy);
	void removeVacancyByIndex(unsigned int vacancy_index);

	std::vector<Unemployed> getUnemployeds() const;
	void addUnemployed(Unemployed unemployed);
	void removeUnemployedByIndex(unsigned int unemployed_index);

	void clearAll();

private:
	Database() {}; // ����� ��� ��������
	~Database() {}; // ����� ��� ��������

	Database(Database const&) = delete; // ����� ��� ��������
	Database& operator= (Database const&) = delete; // ����� ��� ��������

	std::vector<Vacancy> vacancies_; // ���� ��������
	std::vector<Unemployed> unemployeds_; // ���� �����������
};

