#pragma once
#include <string>
#include "Enums.h"
#include "Unemployed.h"

class Vacancy
{

public:
	Vacancy();
	Vacancy(char* company_name, char* vacant_post, unsigned wage, Sex sex, unsigned age, Education education, unsigned total_exp, unsigned last_exp, tm date_of_add);

	~Vacancy();

	char* getCompanyName();
	void setCompanyName(char* company_name);
	char* getVacantPost();
	void setVacantPost(char* vacant_post);
	unsigned getWage() const;
	void setWage(const unsigned wage);
	Sex getSex() const;
	void setSex(const Sex sex);
	unsigned getAge() const;
	void setAge(const unsigned age);
	Education getEducation() const;
	void setEducation(const Education education);
	unsigned getTotalExp() const;
	void setTotalExp(const unsigned total_exp);
	unsigned getLastExp() const;
	void setLastExp(const unsigned last_exp);
	bool unemployedFit(Unemployed unemployed);

private:
	char company_name_[20];
	char vacant_post_[30];
	unsigned int wage_;
	Sex sex_;
	unsigned int age_;
	Education education_;
	unsigned int total_exp_;
	unsigned int last_exp_;
	tm date_of_add_;
public:
	tm getDateOfAdd() const;
	void setDateOfAdd(const tm& date_of_add);
};

