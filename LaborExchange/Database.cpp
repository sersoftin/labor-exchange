#include "Database.h"

void Database::loadDataFromFile() // �������� ������ �� �����
{
	std::ifstream fin;
	fin.open("vacancies.bin", std::ios::binary); // ��������� ���� � ����������
	if (fin.is_open()) // ���� �������
	{
		while (fin.good()) // ���� ��� ���-�� ���� - ���������
		{
			Vacancy vacancy;
			memset(&vacancy, 0, sizeof(vacancy));
			fin.read(reinterpret_cast<char*>(&vacancy), sizeof(vacancy)); // ����� ���� � ����� �����
			if (fin.eof()) break; // ���� ����� ���-�� ����� �� ���
			vacancies_.push_back(vacancy); // ���� ��� ��, �� ��������� �������� � ������
		}
		fin.close(); // ��������� ����
	}
	else
	{
		throw std::exception("File with vacancies can't be opened."); // ���� ���� �� ������ ������� - ������� ����������
	}
	fin.open("unemployeds.bin", std::ios::binary); // ��������� ���� � ������������
	if (fin.is_open()) // ���� �������
	{
		while(fin.good()) // ���� ��� ���-�� ���� - ���������
		{
			Unemployed unemployed;
			memset(&unemployed, 0, sizeof(unemployed));
			fin.read(reinterpret_cast<char*>(&unemployed), sizeof(unemployed)); // ����� ���� � ����� �����
			if (fin.eof()) break; // ���� ����� ���-�� ����� �� ���
			unemployeds_.push_back(unemployed);  // ���� ��� ��, �� ��������� ������������ � ������
		}
		fin.close(); // ��������� ����
	}
	else
	{
		throw std::exception("File with unemployeds can't be opened."); // ���� ���� �� ������ ������� - ������� ����������
	}
}

void Database::saveDataToFile() // ����� ������ � ����
{
	std::ofstream fout;
	fout.open("vacancies.bin", std::ios::binary); // ��������� ���� � ����������
	if (fout.is_open()) // ���� ������
	{
		for (auto & vacancy : vacancies_) // ����������� �� ������ ��������
		{
			fout.write(reinterpret_cast<char*>(&vacancy), sizeof(vacancy)); // ����� �������� � ����
		}
		fout.close(); // ��������� ����
	}
	else
	{
		throw std::exception("File with vacancies can't be stored."); // ���� ��������� ���� �� �������
	}
	fout.open("unemployeds.bin", std::ios::binary); // ��������� ���� � ������������
	if (fout.is_open()) // ���� �������
	{
		for (auto & unemployed : unemployeds_) // ����������� �� ������ �����������
		{
			fout.write(reinterpret_cast<char*>(&unemployed), sizeof(unemployed)); // ����� ������������ � ����
		}
		fout.close(); // ��������� ����
	}
	else
	{
		throw std::exception("File with unemployeds can't be stored."); // ���� ��������� ���� �� �������
	}
}

void Database::clearAll() // ������� ����
{
	vacancies_.clear(); // ������ ��������
	unemployeds_.clear(); // ������ �����������
}

std::vector<Vacancy> Database::getVacancies() const // ������� ������ ��������
{
	return vacancies_;
}

void Database::addVacancy(Vacancy vacancy)
{
	vacancies_.push_back(vacancy); // ���������� ��������
}

void Database::removeVacancyByIndex(unsigned int vacancy_index) // �������� �������� �� ���� �� �������
{
	if (vacancy_index > 0) // ���� ����� ������ 0
		vacancies_.erase(vacancies_.begin() + vacancy_index - 1); // �������
	else
		throw std::exception("vacancy_index must be > 0"); // ����������� ����������, ���� ������ ������ 0
}

std::vector<Unemployed> Database::getUnemployeds() const // ������� ������ �����������
{
	return unemployeds_;
}

void Database::addUnemployed(Unemployed unemployed) // ��������� ������������
{
	unemployeds_.push_back(unemployed);
}

void Database::removeUnemployedByIndex(unsigned int unemployed_index) // �������� ������������ �� ���� �� �������
{
	if (unemployed_index > 0) // ���� ����� ������ 0
		unemployeds_.erase(unemployeds_.begin() + unemployed_index - 1); // �������
	else
		throw std::exception("unemployed_index must be > 0"); // ����������� ����������, ���� ������ ������ 0
}
