#include "Printer.h"
#include <Windows.h>

std::map<Sex, std::string> sex_strings = { // ���� ��� �������� �������� enum'�� � ������������ �� ������ � �������
	{ Male, "���." },
	{ Female, "���." },
	{ Any, "�����"}
};

std::map<Education, std::string> education_strings = { // ���� ��� �������� �������� enum'�� � ������������ �� ������ � �������
	{ Preschool,  "��" },
	{ PrimaryGeneral, "���" },
	{ BasicGeneral, "���" },
	{ Secondary, "���" },
	{ SecondaryVocation, "���" },
	{ HigherBachelor, "�����������" },
	{ HigherSpecialtyMaster, "������������" },
	{ HigherEducationTrainingOfHighlyQualifiedPersonnel, "����" }
};

void Printer::printUnemployed() // ����� ������ �����������
{
	std::cout << "������ �����������" << std::endl;
	auto unemployeds = Database::getInstance().getUnemployeds(); // �������� ������ �� ����
	std::cout << std::left << std::setw(4) << "#" << std::setw(17) << "�������" << std::setw(6) << "���" << std::setw(10) << "�������" << std::setw(14) << "�����������" << std::setw(20) << "���������" << std::setw(20) << "���������" << std::setw(20) << "���. ���������" << std::setw(10) << "���. �/�" << std::endl; // �������� ���������
	printHorizontalLine(); // �������� �������������� �����
	auto index = 0;
	for (auto & unemployed : unemployeds) // ������� ������ �� �����
	{
		std::cout << std::left << std::setw(4) << ++index << std::setw(17) << unemployed.getFamily() << std::setw(6) << sex_strings[unemployed.getSex()] << std::setw(10) << unemployed.getAge() << std::setw(14) << education_strings[unemployed.getEducation()] << std::setw(20) << unemployed.getProfession() << std::setw(20) << unemployed.getPost() << std::setw(10) << std::setw(20) << unemployed.getWantPost() << unemployed.getWantWage() << std::endl;
	}
	printHorizontalLine(); // �������� �������������� �����
}
 
void Printer::printVacancies() // ����� ������ ��������
{
	std::cout << "������ ��������" << std::endl;
	auto vacancies = Database::getInstance().getVacancies(); // �������� ������ �� ����
	std::cout << std::left << std::setw(5) << "#" << std::setw(17) << "��������" << std::setw(20) << "���������" << std::setw(10) << "�/�" << std::setw(9) << "���" << std::setw(11) << "�������" << std::setw(20) << "�����������" << std::setw(15) << "����� ����" << std::setw(10) << "����. ����" << std::endl;  // �������� ���������
	printHorizontalLine(); // �������� �������������� �����
	auto index = 0;
	for (auto & vacancy : vacancies) // ������� ������ �� �����
	{
		std::cout << std::left << std::setw(5) << ++index << std::setw(17) << vacancy.getCompanyName() << std::setw(20) << vacancy.getVacantPost() << std::setw(10) << vacancy.getWage() << std::setw(9) << sex_strings[vacancy.getSex()] << std::setw(11) << vacancy.getAge() << std::setw(20) << education_strings[vacancy.getEducation()] << std::setw(15) << vacancy.getTotalExp() << std::setw(10) << vacancy.getLastExp() << std::endl;
	}
	printHorizontalLine(); // �������� �������������� �����
}

void Printer::printDemandedProfessions()
{
	std::cout << "C����� �������������� ���������" << std::endl;
	std::map<std::string, int> rainting; // ���� � ����������. ���� - ������������ ���������, �������� - ����� ���������� � ������ �����������
	auto vacancies = Database::getInstance().getVacancies();
	for (auto & vacancy : vacancies)
	{
		++rainting[std::string(vacancy.getVacantPost())]; // ����������� ���������� ���������, � ������ ���������
	}
	std::cout << std::left << std::setw(30) << "���������" << std::setw(20) << "���������� ��������" << std::endl; // �������� ��������� �������
	printHorizontalLine(); // �������� �������������� �����
	std::vector<std::pair<std::string, int>> raiting_vector(rainting.begin(), rainting.end()); // �������� ������ �� ���� � ������ � �������� ��� �� ������ ��������. � ������� ������ ����� ����������� ��� ������
	std::sort(raiting_vector.begin(), raiting_vector.end(), DemandedComparator()); // ��������� ������
	auto rainting_len = raiting_vector.size() > 5 ? 5 : raiting_vector.size();
	for (auto i = 0; i < rainting_len; ++i)
	{
		std::cout << std::left << std::setw(30) << raiting_vector[i].first << std::setw(20) << raiting_vector[i].second << std::endl;
	}
	printHorizontalLine(); // �������� �������������� �����
}

void Printer::printNotDemandedProfessions()
{
	std::cout << "������ ���������������� ���������" << std::endl;
	std::map<std::string, int> rainting; // ���� � ����������. ���� - ������������ ���������, �������� - ����� ���������� � ������ �����������
	auto vacancies = Database::getInstance().getVacancies();
	for (auto & vacancy : vacancies)
	{
		++rainting[std::string(vacancy.getVacantPost())]; // ����������� ���������� ���������, � ������ ���������
	}
	std::cout << std::left << std::setw(30) << "���������" << std::setw(20) << "���������� ��������" << std::endl; // �������� ��������� �������
	printHorizontalLine(); // �������� �������������� �����
	std::vector<std::pair<std::string, int>> raiting_vector(rainting.begin(), rainting.end()); // �������� ������ �� ���� � ������ � �������� ��� �� ������ ��������. � ������� ������ ����� ����������� ��� ������
	std::sort(raiting_vector.begin(), raiting_vector.end(), NotDemandedComparator()); // ��������� ������
	auto rainting_len = raiting_vector.size() > 5 ? 5 : raiting_vector.size();
	for (auto i = 0; i < rainting_len; ++i)
	{
		std::cout << std::left << std::setw(30) << raiting_vector[i].first << std::setw(20) << raiting_vector[i].second << std::endl;
	}
	printHorizontalLine(); // �������� �������������� �����
}

void Printer::printUnfilledVacancies(unsigned int days_passed)
{
	// ����� �� ��������. 
	// ���������� �� ������ �������� � ��������� - ���� �� ����������� �� ��� ��������. ���� ���, �� �������� ������������� ���������� �������������.
	// ���� ������� ����(� �� ����� ���� ���������), �� ���������� �������� ����� ������ ������� ������� ������ ����� ���� � ����� ���
	// �������� ��������� �������� ��� ���(���������� ���� �������� � ���������)

	std::cout << "������ ������������� �������� ��������� �� ��������� " << days_passed << " ����" << std::endl;
	auto vacancies = Database::getInstance().getVacancies(); // �������� ������ �������� �� ��
	auto unemployeds = Database::getInstance().getUnemployeds();

	auto days_passed_seconds = 60 * 60 * 24 * days_passed; // ��������� ���������� ������ � days_passed
	std::cout << std::left << std::setw(5) << "#" << std::setw(17) << "��������" << std::setw(20) << "���������" << std::setw(10) << "�/�" << std::setw(9) << "���" << std::setw(11) << "�������" << std::setw(20) << "�����������" << std::setw(15) << "����� ����" << std::setw(10) << "����. ����" << std::endl;
	printHorizontalLine(); // �������� �������������� �����
	auto found = false;
	for (auto & vacancy : vacancies) // ���������� �� ������ ��������
	{
		auto vacancy_tm = vacancy.getDateOfAdd();
		auto index = 0;
		found = false;
		for (auto & unemployed : unemployeds)
		{
			found = false;
			if (vacancy.unemployedFit(unemployed))
			{
				found = true;
				auto unemployed_tm = unemployed.getDateOfAdd();
				auto diff_seconds = difftime(mktime(&unemployed_tm), mktime(&vacancy_tm));

				if (diff_seconds < 0) // ���� ����� �������������, �� ������ ��� �������������
					diff_seconds *= -1;

				if (diff_seconds > days_passed_seconds) // ���� ���������� ������ ����� �������� ����_����������_�������� � ����_����������_������������ ������, ��� ��������� ����������
				{
					std::cout << std::left << std::setw(5) << ++index << std::setw(17) << vacancy.getCompanyName() << std::setw(20) << vacancy.getVacantPost() << std::setw(10) << vacancy.getWage() << std::setw(9) << sex_strings[vacancy.getSex()] << std::setw(11) << vacancy.getAge() << std::setw(20) << education_strings[vacancy.getEducation()] << std::setw(15) << vacancy.getTotalExp() << std::setw(10) << vacancy.getLastExp() << std::endl;
				}
			}
		}
		if (!found)
			std::cout << std::left << std::setw(5) << ++index << std::setw(17) << vacancy.getCompanyName() << std::setw(20) << vacancy.getVacantPost() << std::setw(10) << vacancy.getWage() << std::setw(9) << sex_strings[vacancy.getSex()] << std::setw(11) << vacancy.getAge() << std::setw(20) << education_strings[vacancy.getEducation()] << std::setw(15) << vacancy.getTotalExp() << std::setw(10) << vacancy.getLastExp() << std::endl;
	}
	printHorizontalLine(); // �������� �������������� �����*/
}

void Printer::printVacanciesByFamily()
{
	std::cin.ignore(); // ����� ��������� ���������� ���� ������������
	std::cout << "������� ������� ������������: ";
	std::string family;
	std::getline(std::cin, family);
	std::cout << "������ �������� ��� " << family << std::endl;
	auto vacancies = Database::getInstance().getVacancies();
	auto unemployeds = Database::getInstance().getUnemployeds();
	Unemployed target_unemployed;
	for (auto & unemployed : unemployeds)
	{
		if (!strcmp(family.c_str(), unemployed.getFamily()))
		{
			target_unemployed = unemployed;
		}
	}
	std::cout << std::left << std::setw(5) << "#" << std::setw(17) << "��������" << std::setw(20) << "���������" << std::setw(10) << "�/�" << std::setw(9) << "���" << std::setw(11) << "�������" << std::setw(20) << "�����������" << std::setw(15) << "����� ����" << std::setw(10) << "����. ����" << std::endl;  // �������� ���������
	printHorizontalLine(); // �������� �������������� �����
	auto index = 0;
	for (auto & vacancy : vacancies)
	{
		++index;
		if (vacancy.unemployedFit(target_unemployed)) // ���� �������� ������������� ����������� ������������ ������������
		{
			std::cout << std::left << std::setw(5) << index << std::setw(17) << vacancy.getCompanyName() << std::setw(20) << vacancy.getVacantPost() << std::setw(10) << vacancy.getWage() << std::setw(9) << sex_strings[vacancy.getSex()] << std::setw(11) << vacancy.getAge() << std::setw(20) << education_strings[vacancy.getEducation()] << std::setw(15) << vacancy.getTotalExp() << std::setw(10) << vacancy.getLastExp() << std::endl;
		}
	}
	printHorizontalLine(); // �������� �������������� �����
}

void Printer::addVacancy() // ���������� ��������
{
	std::cin.ignore(); // ����� ��������� ���������� ���� ������������
	std::cout << "���������� ��������" << std::endl;
	Vacancy vacancy; // ������� ������� ��������
	std::cout << "�������� �����:";
	std::cin.getline(vacancy.getCompanyName(), 20); // �������� ��� �������� �� �������
	std::cout << "������� �������� ��������:";
	std::cin.getline(vacancy.getVacantPost(), 20); // �������� ������������ ��������� �� �������
	std::cout << "������� �/�:";
	auto wage = 0;
	std::cin >> wage; // ��������� �� �� �������
	vacancy.setWage(wage); // ������������� �� ��������
	std::cout << "�������� ���" << std::endl;
	for (auto & sex_string : sex_strings) // �������� ������ ����� ��� ��������
	{
		std::cout << sex_string.first << " - " << sex_string.second << std::endl;
	}
	auto sex = 0;
	std::cin >> sex;
	vacancy.setSex(Sex(sex)); // ������������� ���, ������� ��� ������������
	std::cout << "������� ��������� �������:";
	auto age = 0;
	std::cin >> age; // ��������� �������, ������� ������������ ���
	vacancy.setAge(age); // ������������� ������� ��� ��������
	std::cout << "������� �����������" << std::endl;
	for (auto & education_string : education_strings) // ������� ������ �����������
	{
		std::cout << education_string.first << " - " << education_string.second << std::endl;
	}
	auto education = 0;
	std::cin >> education;
	vacancy.setEducation(Education(education)); // ������������� �����������, �������� �������������
	std::cout << "������� ��������� ������ ����:";
	auto total_exp = 0;
	std::cin >> total_exp;
	vacancy.setTotalExp(total_exp); // ������������� ������ ����, ��������� �������������
	std::cout << "������� ��������� ���� �� ��������� ���������:";
	auto last_exp = 0;
	std::cin >> last_exp;
	vacancy.setLastExp(last_exp); // ������������� ��������� ����, �������� �������������
	auto current_unixtime = time(nullptr); // �������� ������� ����� � unix-time
	auto local_time = localtime(&current_unixtime); // ��������� unix-time � ��������� tm ��� ����������� ������
	tm local_time_str; // ������� ��� ���� ����� �� ��������� ��� �������� ���� � ��������
	memset(&local_time_str, 0, sizeof(local_time_str)); // ��������� ��� ��������� ������
	memcpy(&local_time_str, local_time, sizeof(local_time_str)); // �������� ���� ��, ��� ������� ������� ���� (� ����� ������� �������� � �����)
	vacancy.setDateOfAdd(local_time_str); // ������������ ���� ���������� ������ � ��
	Database::getInstance().addVacancy(vacancy); // ��������� �������� � ��
}

void Printer::addUnemployed() // ���������� � �����������
{
	std::cin.ignore(); // ����� ��������� ���������� ���� ������������
	std::cout << "���������� ������������" << std::endl;
	Unemployed unemployed; // ������� ������� ������������
	std::cout << "������� �������:";
	std::cin.getline(unemployed.getFamily(), 20); // ��������� ������� �� �������
	std::cout << "�������� ���" << std::endl;
	for (auto & sex_string : sex_strings) // ������� ������ �����
	{
		if (sex_string.first != Sex::Any) // ���� ��� �� "�����" (�� ������ ��� ��������)
			std::cout << sex_string.first << " - " << sex_string.second << std::endl;
	}
	auto sex = 0;
	std::cin >> sex; // ��������� ���� ������������
	unemployed.setSex(Sex(sex)); // ������������� ���
	std::cout << "������� �������:";
	auto age = 0;
	std::cin >> age;
	unemployed.setAge(age); // ������������� �������
	std::cout << "������� �����������" << std::endl;
	for (auto & education_string : education_strings) // ������� ������ �����������
	{
		std::cout << education_string.first << " - " << education_string.second << std::endl;
	}
	auto education = 0;
	std::cin >> education;
	unemployed.setEducation(Education(education)); // ������������� ����������� ������������
	std::cin.ignore(); // ����� ��������� ���������� ���� ������������
	std::cout << "������� ���������: ";
	std::cin.getline(unemployed.getProfession(), 29); // ��������� ��������� �� �������
	std::cout << "������� ���������: ";
	std::cin.getline(unemployed.getPost(), 29); // ��������� ��������� �� �������
	std::cout << "������� ���� ������: ";
	auto total_exp = 0;
	std::cin >> total_exp;
	unemployed.setTotalExp(total_exp); // ������ ������ ���� ������������
	std::cout << "������� ���� ������ �� ��������� ���������: ";
	auto last_exp = 0;
	std::cin >> last_exp;
	unemployed.setLastExp(last_exp); // ������ ���� ��������� ��������� ������������
	std::cin.ignore();
	std::cout << "������� �������� ���������: ";
	std::cin.getline(unemployed.getWantPost(), 29); // ��������� ���������, ������� ����� ����������� �� �������
	std::cout << "������� �������� ���������� �����: ";
	auto want_wage = 0;
	std::cin >> want_wage;
	unemployed.setWantWage(want_wage);
	auto current_unixtime = time(nullptr); // �������� ����� � unix-time
	auto local_time = localtime(&current_unixtime); // ��������� � ��������� tm � ������ �������� �����
	tm local_time_str;
	memset(&local_time_str, 0, sizeof(local_time_str)); // ��������� ����� ������ ��� ��������� ������
	memcpy(&local_time_str, local_time, sizeof(local_time_str)); // �������� ��������� ��� ����, ����� �������� �� � ����� ������������
	unemployed.setDateOfAdd(local_time_str); // �������� ��������� � ����� ������������
	Database::getInstance().addUnemployed(unemployed); // ��������� ������������ � ��
}

void Printer::removeVacancy()
{
	std::cout << "������� ����� ��������, ������� �� ������ �������:";
	std::cin.ignore(); // ����� ��������� �� ���������
	auto index = 0;
	std::cin >> index;
	std::cout << "������ " << index << " ������" << std::endl;
	try
	{
		Database::getInstance().removeVacancyByIndex(index); // ������� �� ������� � ������� ��������
		std::cout << "������ " << index << " ������� �������." << std::endl;
	}
	catch (std::exception & ex)
	{
		std::cout << "��� �������� ������ ��������� ������: " << ex.what() << std::endl; // ����� ����������, ���� �������� �� �������
	}
}

void Printer::removeUnemployed()
{
	std::cout << "������� ����� ������������, �������� �� ������ �������:";
	std::cin.ignore(); // ����� ��������� �� ���������
	auto index = 0;
	std::cin >> index;
	std::cout << "������ #" << index << " ������������" << std::endl;
	try
	{
		Database::getInstance().removeVacancyByIndex(index);
		std::cout << "����������� #" << index << " ������� �������." << std::endl;
	}
	catch (std::exception & ex)
	{
		std::cout << "��� �������� ������������ ��������� ������: " << ex.what() << std::endl; // ����� ����������, ���� �������� �� �������
	}
}

void Printer::printHorizontalLine() // ������ �������� �������������� ����� ��� ������
{
	std::cout << "-----------------------------------------------------------------------------------------------------------------------" << std::endl;
}

void Printer::printMenu() // ������ ����
{
	std::cout << "�������� ����� ����:" << std::endl;

	std::cout << "1. ������� ������ �����������" << std::endl;
	std::cout << "2. �������� ������������" << std::endl;
	std::cout << "3. ������� ������������" << std::endl;

	std::cout << "4. ������� ������ ��������" << std::endl;
	std::cout << "5. �������� ��������" << std::endl;
	std::cout << "6. ������� ��������" << std::endl;

	std::cout << "7. ��������� ������� ����� �������������� ���������" << std::endl;
	std::cout << "8. ��������� ������� ����� ���������������� ���������" << std::endl;
	std::cout << "9. ��������� ������� ������������� �������� ����� 1 ������" << std::endl;
	std::cout << "10. ��������� ������� ������������� �������� ����� 1 ������" << std::endl;
	std::cout << "11. ������ �������� �� ������� ������������" << std::endl;

	std::cout << "12. �������� ����" << std::endl;
	std::cout << "13. ��������� ��� ������ � �����" << std::endl;
	std::cout << "14. �������� ��� ������ �� ����" << std::endl;

	std::cout << "0. �����" << std::endl;
}
