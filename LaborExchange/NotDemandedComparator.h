#pragma once
#include <string>

class NotDemandedComparator // компаратор для сортировки в невостребованном рейтинге
{
public:
	bool operator()(std::pair<std::string, int> const& left, std::pair<std::string, int> const& right) const
	{
		return left.second < right.second;
	}
};

