#pragma once
#include "Database.h"
#include <iostream>
#include <iomanip>
#include <map>
#include <algorithm>
#include "DemandedComparator.h"
#include "NotDemandedComparator.h"


class Printer
{
public:
	static void printMenu();

	static void printUnemployed();
	static void printVacancies();

	static void printDemandedProfessions();
	static void printNotDemandedProfessions();
	static void printUnfilledVacancies(unsigned int days_passed);
	static void printVacanciesByFamily();

	static void addVacancy();
	static void addUnemployed();

	static void removeVacancy();
	static void removeUnemployed();

private:
	static void printHorizontalLine();
};

