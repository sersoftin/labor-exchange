#include "Vacancy.h"

Vacancy::Vacancy()
	: wage_(0),
	sex_(), 
	age_(0), 
	education_(), 
	total_exp_(0), 
	last_exp_(0)
{
	// ������� ������ �� ��������
	memset(company_name_, 0, sizeof(company_name_));
	memset(vacant_post_, 0, sizeof(vacant_post_));
}

Vacancy::Vacancy(char* company_name, char* vacant_post, unsigned wage, Sex sex, unsigned age, Education education, unsigned total_exp, unsigned last_exp, tm date_of_add)
	: wage_(wage),
	sex_(sex),
	age_(age),
	education_(education),
	total_exp_(total_exp),
	last_exp_(last_exp),
	date_of_add_(date_of_add)
{
	// ������� ������ �� ��������
	memset(company_name_, 0, sizeof(company_name_));
	memset(vacant_post_, 0, sizeof(vacant_post_));

	// �������� ��������� �� ������������ � ��������� ������
	memcpy(company_name_, company_name, strlen(company_name_));
	memcpy(vacant_post_, vacant_post, strlen(vacant_post_));
}

Vacancy::~Vacancy()
{
}

char* Vacancy::getCompanyName() // ��������� ����� ��������
{
	return company_name_;
}

void Vacancy::setCompanyName(char* company_name) // ��������� ����� ��������
{
	memset(company_name_, 0, sizeof(company_name_)); // ��������� ������ �������� �������
	memcpy(company_name_, company_name, strlen(company_name)); // �������� ������ �� ���������� � ���������
}

char* Vacancy::getVacantPost() // ������� ��������� �����
{
	return vacant_post_;
}

void Vacancy::setVacantPost(char* vacant_post) // ��������� ���������� �����
{
	memset(vacant_post_, 0, sizeof(vacant_post_));
	memcpy(vacant_post_, vacant_post, strlen(vacant_post));
}

unsigned Vacancy::getWage() const // ��������� �������� ��
{
	return wage_;
}

void Vacancy::setWage(const unsigned wage) // ��������� �������� ��
{
	wage_ = wage;
}

Sex Vacancy::getSex() const // ��������� ����
{
	return sex_;
}

void Vacancy::setSex(const Sex sex) // ��������� ����
{
	sex_ = sex;
}

unsigned Vacancy::getAge() const // ��������� ��������
{
	return age_;
}

void Vacancy::setAge(const unsigned age) // ��������� ��������
{
	age_ = age;
}

Education Vacancy::getEducation() const // ��������� �����������
{
	return education_;
}

void Vacancy::setEducation(const Education education) // ��������� �����������
{
	education_ = education;
}

unsigned Vacancy::getTotalExp() const // ��������� ������� �����
{
	return total_exp_;
}

void Vacancy::setTotalExp(const unsigned total_exp) // ��������� ������� �����
{
	total_exp_ = total_exp;
}

unsigned Vacancy::getLastExp() const // ��������� ���������� �����
{
	return last_exp_;
}

void Vacancy::setLastExp(const unsigned last_exp) // ��������� ���������� �����
{
	last_exp_ = last_exp;
}

bool Vacancy::unemployedFit(Unemployed unemployed)
{
	return !strcmp(unemployed.getWantPost(), getVacantPost())
		&& unemployed.getAge() >= getAge() // ���� �������� �� ��������
		&& unemployed.getEducation() >= getEducation() // ���� �������� �� ����������� ��� ����� ����� "������" �����������
		&& (unemployed.getSex() == getSex() || getSex() == Sex::Any) // ���� ��������� ��������� ��� ��� �� � �������� ��� �� �����
		&& unemployed.getTotalExp() >= getTotalExp() // ���� ����� ����� �� ��� ������� ���� ������, ��� �����
		&& unemployed.getLastExp() >= getLastExp() // ���� ����� ����� �� ��� ������� ���� ������ �� ��������� ���������, ��� �����
		&& unemployed.getWantWage() <= getWage();
}

tm Vacancy::getDateOfAdd() const // ��������� ���� ����������
{
	return date_of_add_;
}

void Vacancy::setDateOfAdd(const tm& date_of_add) // ��������� ���� ����������
{
	date_of_add_ = date_of_add;
}
